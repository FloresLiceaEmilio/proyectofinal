import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, ref, onValue, set, onChildAdded, onChildRemoved, remove, update, onChildChanged } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';

// Configuración de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyA-dZBKEAAZJL7pJQfch5Z56hreqtYjv8M",
  authDomain: "proyectofinalemilioflores.firebaseapp.com",
  databaseURL: "https://proyectofinalemilioflores-default-rtdb.firebaseio.com",
  projectId: "proyectofinalemilioflores",
  storageBucket: "proyectofinalemilioflores.appspot.com",
  messagingSenderId: "871797425068",
  appId: "1:871797425068:web:acd5c3297bfc026f97f009"
};


const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);
const dbRef = ref(db);


onValue(dbRef, (snapshot) => {
  const datos = snapshot.val();

  // Verificar si hay datos
  if (datos) {
    const cantidadLlaves = Object.keys(datos).length;
    console.log(`Cantidad de llaves primarias: ${cantidadLlaves}`);
  } else {
    console.log('No hay datos en la base de datos.');
  }
});

// Referencia a la base de datos en tiempo real de Firebase
function mostrarProductos(){

  const container = document.getElementById('productos-container');

      const productosRef = ref(db, 'Productos/');
      onChildAdded(productosRef, snapshot => {
        console.log(`Añadiendo producto con ID ${snapshot.key}`);

        const data = snapshot.val();
        const div = document.createElement("div");
        div.classList.add('bg')
        // Crear y añadir la imagen
        const imgcenter = document.createElement("div");
        const img = document.createElement("img");
        imgcenter.classList.add('center');
        img.src = data.ImagenURL;
        imgcenter.appendChild(img);
        div.appendChild(imgcenter);
        // Crear y añadir el nombre del producto
        const h1Title = document.createElement("h4");
        h1Title.textContent = data.Nombre;

        div.appendChild(h1Title);

        // Crear y añadir el precio

        const pPrice = document.createElement("p");
        pPrice.textContent = `$${data.Precio}`;
        pPrice.classList.add('precio')
        div.appendChild(pPrice);

        // Crear y añadir la descripción

        const pDescription = document.createElement("p");
        pDescription.textContent = data.Descripcion;
        pDescription.classList.add('descripcion')
        div.appendChild(pDescription);

        //Crear y añadir la cantidad en stock
        const cantidad = document.createElement("p");
        cantidad.textContent = "Cantidad en stock: " + data.Cantidad;
        cantidad.classList.add('cantidad')
        div.appendChild(cantidad);

    
        container.classList.add('grid');
        container.appendChild(div);

    });

    
};

mostrarProductos();
