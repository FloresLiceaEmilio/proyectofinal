import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, ref, get,set, onChildAdded, onChildRemoved, remove, update, onChildChanged } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyA-dZBKEAAZJL7pJQfch5Z56hreqtYjv8M",
    authDomain: "proyectofinalemilioflores.firebaseapp.com",
    databaseURL: "https://proyectofinalemilioflores-default-rtdb.firebaseio.com",
    projectId: "proyectofinalemilioflores",
    storageBucket: "proyectofinalemilioflores.appspot.com",
    messagingSenderId: "871797425068",
    appId: "1:871797425068:web:acd5c3297bfc026f97f009"
  };

const app = initializeApp(firebaseConfig);

const db = getDatabase(app);
const storage = getStorage(app);


//elementos del formulario
    const nombre = document.getElementById('nombre');
    const precio = document.getElementById('precio');
    const imagenURL = document.getElementById('imagenGato');
    const descripcion = document.getElementById('descripcion');
    const cantidad = document.getElementById('cantidad');
    const IdProducto = document.getElementById('IdProducto');

    const nombre2 = document.getElementById('nombre2');
    const precio2 = document.getElementById('precio2');
    const imagenURL2 = document.getElementById('imagenGato2');
    const descripcion2 = document.getElementById('descripcion2');
    const cantidad2 = document.getElementById('cantidad2');
    const IdProducto2 = document.getElementById('IdProducto2');

//Funcion de limpieza después de añadir
function resetForm() {
    document.getElementById('IdProducto').value = '';
    document.getElementById('nombre').value = '';
    document.getElementById('precio').value = '';
    document.getElementById('descripcion').value = '';
    document.getElementById('cantidad').value = '';
    document.getElementById('imagenGato').value = '';
  }
  
  function storeData() {
    // Obtener el archivo de imagen seleccionado por el usuario
    const imagenFile = imagenURL.files[0];
  
    // Verificar si se seleccionó un archivo
    if (!imagenFile) {
      alert("Por favor, selecciona una imagen.");
      return;
    }
  
    // Crear una referencia única para el archivo de imagen en Storage
    const imagenRef = storageRef(storage, 'imagenes/' + imagenFile.name);
  
    // Subir el archivo de imagen a Storage
    const uploadTask = uploadBytesResumable(imagenRef, imagenFile);
  
    uploadTask
      .then((snapshot) => getDownloadURL(snapshot.ref)) // Obtener la URL de descarga después de que la carga se completa
      .then((url) => {
        const nuevoProducto = {
          Nombre: nombre.value,
          Precio: precio.value,
          Descripcion: descripcion.value,
          Cantidad: cantidad.value,
          ImagenURL: url
        };
  
        return set(ref(db, "Productos/" + IdProducto.value), nuevoProducto);
      })
      .then(() => {
        alert("Datos guardados correctamente");
        resetForm();
      })
      .catch((error) => {
        console.error("Error al agregar el producto:", error);
        alert("No se pudo guardar los datos" + error);
      });
  }
imagenURL.addEventListener('change', function() {
  });
  
  
  const form = document.getElementById('anadirProductos');

  form.addEventListener('submit', function (e) {
    e.preventDefault(); // Evitar que el formulario se envíe
  
    // Lógica de añadir producto
    storeData();
  });
  
//Funcion para actualizar datos

function UpdateData(){
  const imagenFile = imagenURL2.files[0];
  

    if (!imagenFile) {
      alert("Por favor, selecciona una imagen.");
      return;
    }
  

    const imagenRef = storageRef(storage, 'imagenes/' + imagenFile.name);
  

    const uploadTask = uploadBytesResumable(imagenRef, imagenFile);
  
    uploadTask
      .then((snapshot) => getDownloadURL(snapshot.ref)) 
      .then((url) => {
        const nuevoProducto = {
          Nombre: nombre2.value,
          Precio: precio2.value,
          Descripcion: descripcion2.value,
          Cantidad: cantidad2.value,
          ImagenURL: url
        };
  
        return update(ref(db, "Productos/" + IdProducto2.value), nuevoProducto);
      })
      .then(() => {
        alert("Datos actualizados correctamente");
        resetForm();
      })
      .catch((error) => {
        console.error("Error al actualizar el producto:", error);
        alert("No se pudo actualizar los datos" + error);
      });
  
  
  
}

const formedit = document.getElementById('editarProductos');

  formedit.addEventListener('submit', function (e) {
    e.preventDefault(); // Evitar que el formulario se envíe
    UpdateData()
  });


  //funcion para borrar datos
  function DeleteData(){ 
        remove(ref(db, "Productos/" + IdProducto3.value))
        .then(() => {
          alert("Datos borrados correctamente");
          resetForm();
        })
        .catch((error) => {
          console.error("Error al borrar el producto:", error);
          alert("No se pudo borrar los datos" + error);
        });
    
    
    
  }
  
  const formdelete = document.getElementById('borrarProductos');
  
    formdelete.addEventListener('submit', function (e) {
      e.preventDefault(); // Evitar que el formulario se envíe
      DeleteData()
    }); 